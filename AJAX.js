let API_KEY = "05b3491fff7dfd9c4b785848290e7553";
const uri = "http://data.fixer.io/api/latest?access_key=" + API_KEY;

$(document).ready(function () {

    $("#count, #from, #to").on("change keypress", function () {
        $.ajax({
            url: uri
        }).then(function (res) {
            let from = $("#from").val().toUpperCase();
            let to = $("#to").val().toUpperCase();
            let count = $("#count").val();
            let tmp = res.rates[to] / res.rates[from] * count;

            if (tmp) {
                $("#output").val(tmp.toFixed(2));
            }
        }, function (res) {
            console.log(res);
        })

    });
})
;
